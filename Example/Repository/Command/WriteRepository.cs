﻿using Example.Model;
using Example.Repository.Command.Interfaces;

namespace Example.Repository.Command
{
    /// <summary>
    /// Klasa generyczna pozwala na utworzenie jednej, uniwersalnej klasy
    /// przy tworzeniu obiektu w miejsce T wprowadzamy nazwę klasy np. WriteRepository<Student>
    /// Dzięki czemu w miejsce każdego T będziemy mieli klasę Student
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class WriteRepository<T> : IWriteRepository<T> where T: Entity
    {
        private readonly DatabaseContext _context;
        public WriteRepository(DatabaseContext context)
        {
            _context = context;
        }

        public void Delete(T entity)
        {
            _context.Set<T>().Remove(entity);
        }

        public void Save(T entity)
        {
            _context.Set<T>().Add(entity);
        }

        public void Update(T entity)
        {
            _context.Entry(entity);
        }
    }
}