﻿using System.Collections.Generic;

namespace Example.Repository.Query.Interfaces
{
    public interface IReadRepository<T> where T: class
    {
        IList<T> GetAll();
        T GetById(int id);
    }
}