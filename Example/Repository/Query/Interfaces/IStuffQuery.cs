﻿using System.Collections.Generic;
using Example.Model;

namespace Example.Repository.Query.Interfaces
{
    public interface IStuffQuery : IReadRepository<Stuff>
    {
        IList<Stuff> GetByStudent(Student student);
    }
}